'use strict';
/*jshint esversion: 6 */

const soap = require('soap');
const crypto = require('crypto');
const md5 = require('md5');
const syslog = require('syslog-client');
const AWS = require('aws-sdk');
//{
//	LPAReference 	: X9999,
//	CustomerName	: 'NameOfCustomer',
//	URLs			: {
//						Applications:'WSDL URL for applications',
//						Files:'WSDL URL for files'
//						}
//	ProposalRef 	: '123546',
//	BucketName  	: 'planningportal-prod',
//	FilePath		: 'Test/X9999/Building Control Applications/123456',
//	RunStatus		: 'Test'
//}

exports.handler = (event, context, callback) => {
	var S3 = new AWS.S3({apiVersion: '2006-03-01'});
	var LAMBDA = new AWS.Lambda({ region: 'eu-west-1' });

	var client = syslog.createClient('10.42.100.22', {
		  syslogHostname: "cmaas-lambda",
		  transport: syslog.Transport.Tcp,
		  tcpTimeout: 2000
		});

	client.on('error', function(err) {
		console.error('Error from syslog network: %s', err);
		context.fail('Error from syslog network: %s', err);
	});

	var options = {
		   facility: syslog.Facility.Local0,
		   severity: syslog.Severity.Notice
	};
	
	function GenerateMD5(input){
		return crypto.createHash('md5').update(input).digest('base64');
	}

	function ecmMessage(message, endProcess){
		console.log(message);
		client.log(message, options, function(error) {
	         if (error) {
	             console.error(error);
	          }
	         if(endProcess){
	        	 if(message.indexOf('FAILED')>0){
	        		 return context.fail(message);
	        	 }else{
	        		 return context.succeed(message);
	        	 }
	         }
	        });
	}
	
	function getFromS3(fileName){
		return new Promise((resolve,reject)=>{
			S3.getObject({Bucket : 'planningportal-prod', Key: fileName.replace('+',' ')} ,function(err,data){
				if(err){
					//REPORT ERROR
					reject('getFromS3.S3getObject.err: '+ err);
				}else{
					try{
						let file = data.Body.toString('ascii');
						resolve(file);
					}catch(e)
					{
						reject('getFromS3.parseDataToString.err: '+ err);
					}
				}
			});
		});
	}
	
	function parseApplication(){
		let payload = {
			LPAReference 	: event.LPAReference,
			CustomerName	: event.CustomerName,
			ProposalRef 	: event.ProposalRef,
			BucketName  	: 'planningportal-prod',
			FilePath		: event.FilePath,
			Files			: '??????????????????',	
			RunStatus		: event.RunStatus
		};
		
		return LAMBDA.invoke({
            FunctionName: 'bpc-parseapplication',
            Payload: JSON.stringify(payload),
            InvocationType : 'Event'
		}).promise();
	}
	
	function getWSDLFile(wsdl, file){
		return new Promise((res,rej)=>{
			let idArgs = {lpaCode : event.LPAReference};
			wsdl.GetNextRequestId(idArgs,function(err, Id){
				let fileRequest = {
						attachmentId:file,
						LPACode: event.LPAReference,
						RequestId:Id,
						ProposalRef:event.ProposalRef,
						Signature:GenerateMD5(file+event.ProposalRef+event.LPAReference+event.LPAPassword)
				}
				
			});
		});
	}
	
	function getWSDLFiles(wsdlURL, files){

	}

	console.log("--------------------------------------BPC-GETAPPLICATIONDETAIL RUNNING----------------------------------------------");
	console.log("------------------------------------------"+new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')+"---------------------------------------------------");
	console.log(event);
	ecmMessage('Lambda:CMAAS:Integrations:Client: '+event.CustomerName +' - Action: bpc-getapplicationdetail - Status: START <'+event.RunStatus+'>',false);
	
	function getApplicationDetail(){
		soap.createClient(event.URLs.Applications, function(err, wsdl){
			let idArgs = {lpaCode : event.LPAReference};
			wsdl.GetNextRequestId(idArgs,function(err, result){
				let id = result.GetNextRequestIdResult;
				console.log('Request Id: '+id);
				let args = {request:{
						LPACode : event.LPAReference,
						RequestId: id,
						ProposalRef: event.ProposalRef,
						Signature :GenerateMD5(event.ProposalRef+event.LPAReference + id + event.LPAPassword),
						Version: 1.0
						
				}};
				wsdl.GetProposal(args, function(err,proposal){
					//Store Proposal in S3
					let createXML = S3.upload({Bucket : event.BucketName, Key: event.FilePath+'/application.xml', Body: '??????????????????'}).promise();
					
					createXML.then(
							getWSDLFiles(event.URLs.Files,files).then(res=>{	
									parseApplication().then(resolve=>{
											//wsdl.SetProposalReceived
									},
									reject=>ecmMessage('Lambda:CMAAS:Integrations:Client: '+event.CustomerName +' Action: bpc-getapplicationdetail.parseApplication - Status: FAILED <'+event.RunStatus+'>: '+reject,true)
									);
								}, rej=>ecmMessage('Lambda:CMAAS:Integrations:Client: '+event.CustomerName +' Action: bpc-getapplicationdetail.getWSDLFiles - Status: FAILED <'+event.RunStatus+'>: '+rej,true)
							)
					);
				});
			});
		});
	}
	
}